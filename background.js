/*
	Ethan Semella
	Keyholder
	January 2017
*/

chrome.browserAction.onClicked.addListener(function(tab) {
	chrome.tabs.create({url: chrome.extension.getURL('keyholder.html')});
});

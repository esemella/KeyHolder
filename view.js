/*
	Ethan Semella
	Codebook
	January 2017
*/


$(document).ready(function() {
	/*
		prepare page
	*/

	// create encrypt.js worker for asynchronous calls
	var encrypter = new Worker('encrypt.js');

	$('#feedback1').hide();
	$('#feedback2').hide();
	var empty = false;
	refresh();

	// load master password if one is already set
	chrome.storage.local.get('master', function(e) {
		if (isEmpty(e)) {
			empty = true;
			$('#master1').parent().hide();
			$('#new_domain').hide();
		} else {
			empty = false;
			$('#set').html('Reset Master Password');
		}
	});


	/*
		Receive results from encrypt.js
	*/
	encrypter.addEventListener('message', function(e) {
		// master password was not authenticated
		if (e.data.type == "invalid") {
			$('#feedback1').hide();
			$('#feedback2').hide();
			alert("Incorrect Master Password");
		// create new hash for master password and clear all passwords
		} else if (e.data.type == "hash") {
			chrome.storage.local.clear();
			var data = {};
			data["master"] = {
				"hash": e.data.hash,
				"salt": e.data.salt
			};
			chrome.storage.local.set(data);
			refresh();
			$('#feedback1').hide();
			$('#master1').parent().show();
			$('#new_domain').show();
			$('#set').html('Reset Master Password');
			empty = false;
		// create new encrypted password
		} else if (e.data.type == "cipher") {
			var data = {};
			data[e.data.domain] = {
				"username": e.data.username,
				"cipher": e.data.cipher,
				"MAC": e.data.MAC,
				"salt": e.data.salt,
				"IV": e.data.IV
			};
			chrome.storage.local.set(data);
			refresh();
			$('#feedback2').hide();
		}
	}, false);

	/*
		Send sources of entropy to encrypter to use for salts
	*/

	// get random number every 0.5s
	window.setInterval(event, 500);
	function event() {
		var random = Math.random();
		random = random.toString();
		encrypter.postMessage({"task": "event", "source": random});
	}

	// get mouse position on page when mouse moves
	$('body').mousemove(function(e) {
		var msg = e.pageX + e.pageY;
		msg = msg.toString();
		encrypter.postMessage({"task": "event", "source": msg});
	});

	// get id of DOM element that mouse is over
	$('*').mouseover(function() {
		encrypter.postMessage({"task": "event", "source": this.id});
	});

	// get time interval between all keypresses
	var start = 0;
	$('body').keypress(function(e) {
		var elapsed = new Date().getTime() - start;
		start = new Date().getTime();
		elapsed = elapsed.toString();
		encrypter.postMessage({"task": "event", "source": elapsed});
	});

	/*
		Set/Reset master password
	*/
	$('#set').click(function() {
		// check inputs
		var newmaster = $('#newMaster').val();
		var confirmation = $('#confirmMaster').val();
		if (confirmation != newmaster) {
			alert("Passwords do not match");
			newmaster = 0;
			confirmation = 0;
			clear1();
			return;
		}
		if (newmaster == "" || password == "") {
			alert("Master Password, New Master Password, and Confirm New Master Password are required fields");
			newmaster = 0;
			confirmation = 0;
			clear1();
			return;
		}

		// authenticate master if one already exists
		if (!empty) {
			var master = $('#master1').val();
			chrome.storage.local.get('master', function(e) {
				$('#feedback1').show();
				encrypter.postMessage({
					"task": "authenticate",
					"master": master,
					"hash": e.master.hash,
					"salt": e.master.salt,
					"newmaster": newmaster
				});
			});
		// hash new master password
		} else {
			$('#feedback1').show();
			encrypter.postMessage({
				"task": "hash",
				"newmaster": newmaster
			});
		}
		clear1();
	});

	/*
		Create new site entry
	*/
	$('#submit').click(function() {
		// check inputs
		var domain = $('#domain').val();
		domain = getDomain(domain);
		var username = $('#username').val();
		var master = $('#master2').val();
		var password = $('#password').val();
		var confirmPassword = $('#confirmPassword').val();
		if (confirmPassword != password) {
			alert("Passwords do not match");
			password = 0;
			confirmMaster = 0;
			master = 0;
			clear2();
			return;
		}
		if (domain == "" || master == "" || password == "") {
			alert("Site URL, Master Password, and Password are required fields");
			password = 0;
			confirmMaster = 0;
			master = 0;
			clear2();
			return;
		}
		// get hash of master to authenticate inputted master
		chrome.storage.local.get('master', function(e) {
			$('#feedback2').show();
			encrypter.postMessage({
				"task": "encrypt",
				"master": master,
				"hash": e.master.hash,
				"salt": e.master.salt,
				"password": password,
				"username": username,
				"domain": domain
			});
		});
		clear2();
	});
});

// clear New Site
function clear2() {
	$('#master2').val('');
	$('#domain').val('');
	$('#username').val('');
	$('#password').val('');
	$('#confirmPassword').val('');
}

// clear Master Password
function clear1() {
	$('#master1').val('');
	$('#newMaster').val('');
	$('#confirmMaster').val('');
}

// update list of sites created
function refresh() {
	chrome.storage.local.get(null, function(items) {
		$('#domain_list').empty();
		for (var i in items) {
			if (i != 'master') {					// do not display hash of master
				// display username if one was provided
				if (items[i].username != "") {
					$('#domain_list').append(
					'<div class="domain"><h4 class="domain_name">' +
					i + '</h4><p> <strong>Username:</strong><br>' +
					items[i].username + '</p></div>');
				} else {
					$('#domain_list').append(
					'<div class="domain"><h4 class="domain_name">' + i + '</h4><div>');
				}
			}
		}
	});
}

// check if object is empty
function isEmpty(obj) {
	for (var o in obj) {
		if (obj.hasOwnProperty(o)) {
			return false;
		}
	}
	return true;
}

// get domain name from url
function getDomain(url) {
	var domain;
    if (url.indexOf("://") > -1) {
		domain = url.split('/')[2];
	} else {
		domain = url.split('/')[0];
	}
	if (domain.indexOf("www.") > -1) {
		var temp = domain.split('.');
		domain = temp[1];
		for (var i = 2; i < temp.length; i++) {
			domain = domain + "." + temp[i];
		}
	}
	domain = domain.split(':')[0];
	return domain;
}
